package protocal;

import java.io.BufferedReader;
import java.security.MessageDigest;

public class Lib{
	private static BufferedReader in;
	
	public static void init(BufferedReader inn)
	{
		in = inn;
	}
	
	public static String getString(String prompt, int minLength, int maxLength)
	{
		String result = "";
		System.out.println(prompt);
		try{
			while(true)
			{
				result = in.readLine();
				if(result.length() < minLength || result.length() > maxLength)
				{
					System.out.println("Sorry, the length of your input must be with in "+minLength+" to "+maxLength+", please try again: ");
					continue;
				}
				if(!Lib.validString(result))
				{
					System.out.println("Sorry,out of the consideration of safety, you can only input _, 0-9, a-z and A-Z., please try again: ");
					continue;
				}
				break;
			}
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			System.out.println("Not a valid input, System regard that you enter a empty string");
		}
		return result;
	}
	
	public static String getString(String prompt)
	{
		return getString(prompt, 0, 999999);
	}
	
	public static String getString(String prompt, int minLength)
	{
		return getString(prompt, minLength, 999999);
	}
	
	public static int getInt(String prompt, int min, int max)
	{
		System.out.println(prompt);
		String s = null;
		int result = 0;
		boolean valid = false;
		while(!valid)
		{
			try
			{
				while(true)
				{
					s = in.readLine();
					result = Integer.parseInt(s);
					if(result < min || result > max)
					{
						System.out.println("Sorry, the value of your input must be with in "+min+" to "+max+", please try again: ");
						continue;
					}
					valid = true;
					break;
				}
			}
			catch(Exception e)
			{
				valid = false;
				System.out.println("Not a valid input, Please try again!");
				
			}
		}
		return result;
	}
	
	public static int getInt(String prompt)
	{
		return getInt(prompt, 0, 999999);
	}
	public static int getInt(String prompt, int min)
	{
		return getInt(prompt, min, 999999);
	}
	public static void p(String s)
	{
		System.out.println(s);
	}
	
	public static float getFloat(String prompt, float min, float max)
	{
		System.out.println(prompt);
		String s = null;
		float result = 0;
		try
		{
			while(true)
			{
				s = in.readLine();
				result = Float.parseFloat(s);
				if(result < min || result > max)
				{
					System.out.println("Sorry, the value of your input must be with in "+min+" to "+max+", please try again: ");
					continue;
				}
				break;
			}
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			System.out.println("Not a valid input, System regard that you enter 0");
		}
		return result;
	}
	public static int getFloat(String prompt)
	{
		return getInt(prompt, 0, 999999);
	}
	public static int getFloat(String prompt, int min)
	{
		return getInt(prompt, min, 999999);
	}
	
	public static String getString(String prompt, int minLength, int maxLength, String special)
	{

		String res = Lib.getString(prompt, minLength, maxLength);
		if(special.compareTo("format") == 0)
		{
			while(res.compareTo("hardcover")!=0 && res.compareTo("softcover")!=0)
			{
				Lib.p("The format of a book must be either hardcover or softcover.");
				res = Lib.getString(prompt, minLength, maxLength);
			}
		}
		return res;
	}
	
	public static String getMD5(String original)
	{
		char [] toHex = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
		try
		{
		 	MessageDigest md = MessageDigest.getInstance("MD5");
		 	md.update(original.getBytes());
		 	byte [] result = md.digest();
		 	StringBuffer sb = new StringBuffer();
		 	for(int i = 0; i < result.length; ++i)
		 	{
		 			int b = result[i] + 128;
		 			sb.append(toHex[b/16]);
		 			sb.append(toHex[b%16]);
		 	}
		 	return sb.toString();
	 	}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean validString(String s)
	{
		/*
		for(int i = 0; i < s.length(); ++i)
		{
			char c = s.charAt(i);
			if(!Lib.validChar(c))
				return false;
		}*/
		return true;
	}
	
	public static boolean validChar(char c)
	{
		return c == '_' || ( c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') ;
	}

}
