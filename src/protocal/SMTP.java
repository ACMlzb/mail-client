package protocal;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class SMTP {

	private static String endLine  = "\r\n";
	private String server;
	private int port;
	private Socket socket;
	private BufferedReader in;
	private DataOutputStream out;
	
	public boolean debug = false;
	public boolean tap = false;
	
	public SMTP(String server, int port) throws UnknownHostException, IOException
	{
		this.server = server;
		this.port = port;
		this.socket = new Socket(this.server, this.port);
		this.in = new BufferedReader(new InputStreamReader(socket.getInputStream())); 
		this.out = new DataOutputStream(socket.getOutputStream());
	}
	
	public SMTP(String server) throws UnknownHostException, IOException
	{
		this(server, 25);
	}
	
	public boolean check(String res, String prefix, String errorMessage)
	{
		if(!res.startsWith(prefix))
		{
			System.out.println(errorMessage);
			return false;
		}
		return true;
	}
	
	public boolean login(String userName, String password)
	{
		try{
			//connection
			if(!this.check(get(), "220", "Connection failed!"))
				return false;
			
			//hand shaking
			send("HELO "+server+endLine);
			if(!this.check(get(), "250", "Hand shaking failed!"))
				return false;
			
			//log in
			send("AUTH LOGIN"+endLine);
			if(!this.check(get(), "334", "Login failed!"))
				return false;
			
			String msg_name = new String(encode(userName.getBytes())) + endLine;
			send(msg_name);
			if(!this.check(get(), "334", "Login failed, user name error!"))
				return false;

			String msg_password = new String(encode(password.getBytes())) + endLine;
			send(msg_password);
			if(!this.check(get(), "235", "Login failed, password error!"))
					return false;
			
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	public String encode(byte[] b)
	{
		return (new sun.misc.BASE64Encoder()).encode(b);
	}
	public boolean sendEmail(String from, String to, String subject, String content)
	{
		try{
		send("MAIL FROM: <" + from + ">" + endLine);
		if(!this.check(get(), "250", "Error! Invalid email sender: "+from))
			return false;
		
		String [] tos = to.split(",");
		for(String this_to: tos)
		{
			send("RCPT TO: <" + this_to + ">" + endLine);
			if(!this.check(get(), "250", "Error! Invalid email receiver: " + this_to))
				return false;
		}
		send("DATA" + endLine);
		if(!this.check(get(), "354", "Sending content failed!"))
			return false;
		
		String msg = "From: " + from + endLine;
		for(String this_to: tos)
			msg += "To: " + this_to+ endLine;
		
		msg += "Subject: " + subject + endLine;
		msg += endLine;
		msg += content + endLine + "." + endLine;
		send(msg);
		if(!this.check(get(), "250", "Sending content failed"))
			return false;
		return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean close() throws IOException
	{
		try{
			boolean res = true;
		if(debug)
			System.out.println("Quit!");
		send("QUIT" + endLine);
		if(!this.check(get(), "221", "Failed in quit!"))
			res = false;
		this.out.close();
		this.in.close();
		this.socket.close();
		return res;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public void send(String msg) throws IOException
	{
		if(tap)
			System.out.print(" tap->C: "+msg);
		this.out.writeBytes(msg);
		out.flush();
	}
	
	public String get() throws IOException
	{
		String res = this.in.readLine();
		if(tap)
			System.out.println(" tap->S: "+res);
		if(res == null)
			res = "";
		return res;
	}

	public void routine()
	{
		String userName;
		if(debug)
			userName = "13166222065@163.com";
		else
			userName = Lib.getString("user name: ");
		
		String password;
		if(debug)
			password = "123qaz";
		else
			password = Lib.getString("password: ");
		
		if(!this.login(userName, password))
		{
			return;
		}
		Lib.p("Login succeed!");
		String from = userName;
		String to = Lib.getString("Receivers email address, seperated by comma: ");
		String subject = Lib.getString("Subject: ");
		String content = Lib.getString("Content: ");
		boolean res = this.sendEmail(from, to, subject, content);
		if(res)
		{
			Lib.p("Email sending succeed!");
		}
	}
}
