package protocal;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Driver {
	
	public static boolean debug = true;
	public static boolean tap = true;
	
	public static void main(String[] args) {
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		Lib.init(in);
		Lib.p("\t***Welcome to Zhengbo Li's mail client***");
		int d = Lib.getInt("Do you want to enable default input for an easy test? 0-n, 1-y",0,1);
		debug = d == 1;
		int t = Lib.getInt("Do you want to tap the communication between this mail clinet and the server?\n" +
				"If you enable default input I recommend to enable tap so that you can know what is the default input.\n" +
				" 0-n, 1-y",0,1);
		tap = t == 1;
		
		while(true)
		{
			displayMain();
			int c = Lib.getInt("Please enter your choice:", 0, 2);
			if(c == 0)
			{
				Lib.p("Bye!");
				break;
			}
			else if(c == 1)
			{
				String server;
				if(debug)
					server = "smtp.163.com";
				else
					server = Lib.getString("Please enter server name: ");
				SMTP smtp;
				try{
					smtp = new SMTP(server);
					smtp.debug = debug;
					smtp.tap = tap;
				}
				catch(Exception e)
				{
					Lib.p("Server name error!");
					continue;
				}
				
				smtp.routine();
				
				try{
				smtp.close();
				}
				catch(Exception e)
				{
					Lib.p("SMTP close error!");
					continue;
				}
				continue;
			}
			else if(c == 2)
			{
				POP3 pop;
				
				try{
					String server;
					if(debug)
						server = "pop3.163.com";
					else
						server = Lib.getString("Please enter server name: ");
					
					pop = new POP3(server);
					
					pop.debug = debug;
					pop.tap = tap;
				}
				catch(Exception e)
				{
					Lib.p("POP3 constructor error!");
					continue;
				}
				
				pop.routine();
				
				pop.quit();
			}
		}

	}
	
	public static void displayMain()
	{
		Lib.p("\nMain menu: ");
		Lib.p("\t1.Send Email (SMTP)");
		Lib.p("\t2.Check Email (POP3)");
		Lib.p("\t0.QUIT");
	}


}
