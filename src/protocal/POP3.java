package protocal;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedList;

public class POP3 {
	
	private static String endLine  = "\r\n";
	private String server;
	private int port;
	private Socket socket;
	private BufferedReader in;
	private DataOutputStream out;
	
	public boolean debug = false;
	public boolean tap = false;
	
	public POP3(String server, int port) throws UnknownHostException, IOException
	{
		this.server = server;
		this.port = port;
		this.socket = new Socket(this.server, this.port);
		this.in = new BufferedReader(new InputStreamReader(socket.getInputStream())); 
		this.out = new DataOutputStream(socket.getOutputStream());
	}
	
	public POP3(String server) throws UnknownHostException, IOException
	{
		this(server, 110);
	}
	
	public boolean login(String userName, String password)
	{
		try
		{
			if(!this.check(get(), "+OK", "Connection failed!"))
				return false;
			
			send("user " + userName + endLine);
			if(!this.check(get(), "+OK", "Error: invalid user name: "+userName))
				return false;
			
			send("pass " + password + endLine);
			if(!this.check(get(), "+OK", "Error: incorrect password: "+password))
				return false;
			
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean quit()
	{
		try{
			send("quit " + endLine);
			if(!this.check(get(), "+OK", "Quit failed!"))
				return false;
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean deleteMail(int i)
	{
		try{
			send("dele "+i+endLine);
			if(!this.check(get(), "+OK", "Delete mail "+i+" failed!"))
				return false;
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public String getStat()
	{
		String result = null;
		try
		{
			send("stat "+endLine);
			result = get();
			if(!this.check(result, "+OK", "Get stat failed!"))
				return null;
			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public LinkedList<String[]> getList()
	{
		LinkedList<String[]> list = new LinkedList<String[]>();
		try {
			send("list" + endLine);
			if (!check(get(), "+OK", "List error!"))
				return null;
			while(true)
			{
				send("list" + endLine);
				String res = get();
				String [] ress = res.split(" ");
				if(res.equalsIgnoreCase("."))
					break;
				list.add(ress);
			}
			return list;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getMail(int i)
	{
		String result = "";
		try{
		send("retr " + i + endLine);
		if(!this.check(get(), "+OK", "Get mail error"))
			return null;
			while(true)
			{
				String subString = get();
				if(subString.equalsIgnoreCase("."))
					break;
				result += subString;
				result += "\n";
			}
			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public boolean check(String res, String prefix, String errorMessage)
	{
		if(!res.startsWith(prefix))
		{
			System.out.println(errorMessage);
			return false;
		}
		return true;
	}
	
	public void send(String msg) throws IOException
	{
		if(tap)
			System.out.print(" tap->C: "+msg);
		this.out.writeBytes(msg);
		out.flush();
	}
	
	public String get() throws IOException
	{
		String res = this.in.readLine();
		if(tap)
			System.out.println(" tap->S: "+res);
		return res;
	}
	
	public void routine()
	{
		Lib.p("To check your email, please login first!");
		String userName;
		if(debug)
			userName = "13166222065@163.com";
		else userName = Lib.getString("User name: ");
		
		String password;
		if(debug)
			password = "123qaz";
		else
			password = Lib.getString("Password:");
		
		if(!this.login(userName, password))
			return;
		while(true)
		{
			this.displayMenu();
			int c = Lib.getInt("Please enter your choice:",0,5);
			if(c == 0)
				break;
			else if(c == 1) //get stat
			{
				String result = this.getStat();
				if(result != null){
					String [] s = result.split(" ");
					Lib.p(s[1]+" mails, take "+s[2]+" bytes");
				}
			}
			else if(c == 2) //get one mail
			{
				int i = Lib.getInt("Please enter the index of one mail:",1);
				String result = this.getMail(i);
				if(result != null)
					Lib.p(result);
			}
			else if(c == 3)
			{
				int i = Lib.getInt("Please enter the index of one mail:",1);
				boolean res = this.deleteMail(i);
				if(res)
				{
					Lib.p("Delete mail "+i+" successfully!");
				}
			}
			/*
			else if(c == 4)
			{
				this.dirTalk();
			}*/
			
		}
	}
	
	public void displayMenu()
	{
		Lib.p("\nCheck mail menu: ");
		Lib.p("\t0.Back to upper menu");
		Lib.p("\t1.Get total mail number");
		Lib.p("\t2.Get a specific mail");
		Lib.p("\t3.Delete a specific mail");
		//Lib.p("\t4.Talk to the server directly");
		Lib.p("");
	}
	
	public void dirTalk()
	{
		try{
		while(true)
			{
				String in = Lib.getString("C: ");
				if(in.endsWith("q"))
					break;
				send(in + endLine);
				get();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
